<?php


namespace App\DataFixtures;


use App\Entity\Expedition;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ExpeditionsFixtures extends Fixture
{
    private $context = ['Forest', 'Mountain', 'Deep Water', 'Empty'];
    private $event = ['win', 'lost', 'neutral'];
    private $result = ['drop stuff', 'drop gold', 'drop nothing'];

    public function load(ObjectManager $manager)
    {

        for ($i= 0; $i <= 10; $i++) {

            $expedition = new Expedition();

            $expedition->setContext($this->context[rand(0, 3)])
                        ->setEvent($this->event[rand(0, 2)])
                        ->setResult($this->result[rand(0, 2)]);

            $manager->persist($expedition);
        }

        $manager->flush();
    }
}
