<?php

namespace App\Controller;

use App\Service\ExpeditionManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;


class ExpeditionController extends AbstractController
{
    /**
     * @Route("/expedition", name="expedition")
     */
    public function index()
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/ExpeditionController.php',
        ]);
    }

    /**
     * @Route("/goExpe", name="goExpe", methods={"POST"})
     * @param Request $request
     */
    public function generateExpedition(Request $request, ExpeditionManager $expeditionManager)
    {
        $result = $expeditionManager->handleExpe($request->getContent());

        return new JsonResponse($result);
    }
}
